const tasks = [
    { title: 'Faire les courses', isComplete: false },
    { title: 'Nettoyer la maison', isComplete: true },
    { title: 'Planter le jardin', isComplete: false }
];

//utilisation du spread pour cloner le tableau tasks
let tasksCopy = [...tasks];
console.log("Liste des tâches :", tasksCopy);

//Ajouter une tâche
const addTask = (task) => {
    //créer un nouvel objet
    const newTask = {
        title: task,
        isComplete: false,
    };

    //ajoute l'objet au tableau tasksCopy
    tasksCopy.push(newTask);
    return tasksCopy;
}

addTask("Vaisselle"); //ajoute la tâche Vaisselle
console.log("Une tâche a été ajoutée : ", tasksCopy);


//Effacer une tâche 
const removeTask = (titleToRemove) => {
    tasksCopy = tasksCopy.filter(task => task.title !== titleToRemove); //retourne vrai pour les tâches à conserver, renvoyées dans un nouveau tableau tasksCopy (écrase l'ancien)
}

removeTask('Faire les courses'); //efface la tâche
console.log("Une tâche a été effacée : ", tasksCopy);


//Basculer l'état d'une tâche (sans spread)
const toggleTaskStatus = (taskTitleToToggle) => {

    //trouve l'index de l'objet à modifier grâce au titre
    const index = tasksCopy.findIndex(task => task.title === taskTitleToToggle); 

    //vérifie si la propriété isComplete = false
    //si vrai alors bascule sur true : sinon bascule sur false
    tasksCopy[index].isComplete === false ? tasksCopy[index].isComplete = true : tasksCopy[index].isComplete = false;
    console.log("L'état d'une tâche a été modifiée : ", tasksCopy);
}

toggleTaskStatus('Vaisselle'); //switch à true

//afficher les listes de tâches complétées : filtre en vérifiant que isComplete = true. Si vrai, affiche la tâche, sinon ne fait rien (undefined)
const tasksCompleted = () => {
    console.log("Ces tâches ont été complétées :");
    tasksCopy.filter(task => task.isComplete ? console.log(task) : undefined)
}

tasksCompleted();

//afficher les listes de tâches non complétées 
const tasksNotCompleted = () => {
    console.log("Ces tâches n'ont pas encore été complétées :");
    tasksCopy.filter(task => task.isComplete ? undefined : console.log(task))
}

tasksNotCompleted();